

package br.fatecfranca.model;
import java.util.List;
public interface ProfessorDAO {
    public int insere(Professor aluno);
    public int remove(Professor aluno);
    public int atualiza(Professor aluno);
    public List<Professor> consulta();
}
