
package br.fatecfranca.controller;
import br.fatecfranca.model.Professor;
import br.fatecfranca.model.ProfessorDAOImpl;
import java.util.List;
public class ProfessorController {
    
    public int insere(Professor aluno){
        // controller acessando model
        ProfessorDAOImpl alunoDAOImpl = new ProfessorDAOImpl();
        // model retorna dados para controller
        return alunoDAOImpl.insere(aluno);
    }
    
    public List<Professor> consulta(){
        // controller acessando model
        ProfessorDAOImpl alunoDAOImpl = new ProfessorDAOImpl();
        // model retorna dados para controller
        return alunoDAOImpl.consulta();
    }
    
     public int remove(Professor aluno){
        // controller acessando model
        ProfessorDAOImpl alunoDAOImpl = new ProfessorDAOImpl();
        // model retorna dados para controller
        return alunoDAOImpl.remove(aluno);
    }
    
    public int atualiza(Professor aluno){
        // controller acessando model
        ProfessorDAOImpl alunoDAOImpl = new ProfessorDAOImpl();
        // model retorna dados para controller
        return alunoDAOImpl.atualiza(aluno);
    }
     
}
